# README #

## Description: ##

*You can review the example here: - [Demo](http://js-custom-scroll.flexi.ink/). This vertical scroll works on all browsers (ie10+) and mobile devices. It was written on pure js, uses es6 standards. Also, it supports scrolls inclusion.*

-----------------------------------------------------------

## You can install it from npm:

```
npm install js-custom-scroll --save
```

or download from git
```
git clone https://bitbucket.org/flexi-team/js-custom-scroll.git
```

-----------------------------------------------------------


## Example of use:

Include `js-custom-scroll.css` in the head tag your HTML document

```html
<link rel="stylesheet" href="node_modules/dist/js-custom-scroll.css"/>
```

Include `js-custom-scroll.min.js` in the end of your HTML document

```html
<script async src="node_modules/dist/js-custom-scroll.min.js"></script>
```

### Initialize via javascript:

```JavaScript
<script type="text/javascript">
    var _testScroll2 = new jsCustomScroll(elem, {
        at: 80,
        at_Fn: function (scrollbar, el) {
        },
        scrollUp: function (scrollbar, el) {
        },
        scrollDown: function (scrollbar, el) {
        }
    });
    console.log(_testScroll2);
</script>
```

### Properties:

* wheelDelta {number} - Scrolling step. Defaults to `53`.
* speedSwipe {number} - Set the scroll moving speed on swiping.
* minScrollbarLength {number} - The Minimum height of the Scrollbar.
* maxScrollbarLength {number} - The Maximum height of the Scrollbar.
* scrollUp {function} - a Function which will be executed when the scroll is in the zero position.
* scrollDown {function} - a Function which will be executed when the content is fully reviewed. Scroll at the end.
* scrollTopChange {function} - Runs at scrollTop change.
* at {number} - The path achieved in per cent, `at_Fn` sould be called thereafter. Can be used for dynamic content loading.
* at_Fn {function} - Works in conjunction with `at`. If the function doesn't return `true` then it won't be called again. By default it returns `false`.
* draggable {Boolean} - Defaults to `true`: enable mouse drag support in desktop browsers.


## Js-Custom-Scroll API:

Js-Custom-Scroll exposes a few functions that can be useful for script control of your scroll.

* `getScrollbar ()` - Returns the Scrollbar element.
* `getOptions ()` - Returns all the options and additional information about the scroll.
* `setOptions ()` - Options are sent that you want to override after initialization.
* `setScrollTop ()` - Set the position for the scroll.
* `update ()` - Update the scrollbar geometry.
* `kill ()` - Completely remove the Scrollbar.

## License

Copyright (c) 2017 Aleksejj Shmyrko Licensed under the The MIT License (MIT).